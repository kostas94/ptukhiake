#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char ** argv)
{
	srand(time(NULL));
	int i, j, k;
	int filenumber=0;
	char filename[50], tempfilename[50];
	int cars_number;
	FILE * fd;
	if (argc!=2)
	{
		printf("Wrong Arguments\n");
		exit(-1);
	}
	cars_number = atoi(argv[1]);
	sprintf(tempfilename, "cars_base");
	sprintf(filename, "%s.P", tempfilename);

	while((fd=fopen(filename, "r"))!=NULL)
	{
		fclose(fd);
		sprintf(filename, "%s%d.P", tempfilename, filenumber);
		filenumber++;
	}

	if ((fd=fopen(filename, "w"))==NULL)
	{
		printf("Error: at file creation\n");
		exit(-1);
	}

	fprintf(fd, "%% car((id, brand, color, price, hp, used)).\n\n%% %d Cars\n\n\n", cars_number);
	fprintf(fd, ":- hilog return_id, return_brand, return_color, return_price, return_hp, return_used.\n\n");
	fprintf(fd, "return_id((ID,_,_,_,_,_), ID).\n");
	fprintf(fd, "return_brand((_,Brand,_,_,_,_), Brand).\n");
	fprintf(fd, "return_color((_,_,Color,_,_,_), Color).\n");
	fprintf(fd, "return_price((_,_,_,Price,_,_), Price).\n");
	fprintf(fd, "return_hp((_,_,_,_,HP,_), HP).\n");
	fprintf(fd, "return_used((_,_,_,_,_,Used), Used).\n\n\n\n");

	for(i=0; i<cars_number; i++)
	{
		if(i<10)
			fprintf(fd, "car((%d, \t", i);
		else
			fprintf(fd, "car((%d,\t", i);

		j = rand()%10;
		fprintf(fd, "b%d,\t", j);

		j = rand()%10;

		if(j==0)
			fprintf(fd, "white,\t");
		if(j==1)
			fprintf(fd, "black,\t");
		if(j==2)
			fprintf(fd, "blue,\t");
		if(j==3)
			fprintf(fd, "silver,\t");
		if(j==4)
			fprintf(fd, "yellow,\t");
		if(j==5)
			fprintf(fd, "green,\t");
		if(j==6)
			fprintf(fd, "red,\t");
		if(j==7)
			fprintf(fd, "brown,\t");
		if(j==8)
			fprintf(fd, "orange,\t");
		if(j==9)
			fprintf(fd, "grey,\t");
		j = (rand()%46+5)*1000+(rand()%2)*500;

		fprintf(fd, "%d,\t", j);

		j = rand()%340+60;
		fprintf(fd, "%d,\t", j);

		j = rand()%2;
		if (j==0)
			fprintf(fd, "new)).\n");
		else
			fprintf(fd, "used)).\n");
	}

	fclose(fd);
	exit(0);
}
