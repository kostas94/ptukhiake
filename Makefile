OBJS	= create_cars_base.o graph.o
SOURCE 	= create_cars_base.c graph.c
OUT	= create_cars_base graph
CC	= gcc
FLAGS	= -g -c -O3
PARAMS	= -O3



all: $(OBJS)
	$(CC) create_cars_base.o $(PARAMS) -o create_cars_base
	$(CC) graph.o $(PARAMS) -o graph
	rm -f $(OBJS)


create_cars_base.o: create_cars_base.c
	$(CC) $(FLAGS) create_cars_base.c


graph.o: graph.c
	$(CC) $(FLAGS) graph.c


clean:
	rm -f $(OUT)
	rm -f $(OBJS)
	rm -f *.xwam
	rm -f src/*.xwam
	rm -f *~
	rm -f src/*~


clean_files:
	rm -f cars_base*.P
	rm -f graph_paths*.P


clean_xwam:
	rm -f *.xwam
	rm -f src/*.xwam

count:
	wc $(SOURCE)

