% car((id, brand, color, price, hp, used)).

% 75 Cars


:- hilog return_id, return_brand, return_color, return_price, return_hp, return_used.

return_id((ID,_,_,_,_,_), ID).
return_brand((_,Brand,_,_,_,_), Brand).
return_color((_,_,Color,_,_,_), Color).
return_price((_,_,_,Price,_,_), Price).
return_hp((_,_,_,_,HP,_), HP).
return_used((_,_,_,_,_,Used), Used).



car((0, 	b8,	orange,	5500,	106,	used)).
car((1, 	b9,	silver,	32500,	112,	new)).
car((2, 	b2,	grey,	27500,	308,	used)).
car((3, 	b3,	white,	50000,	276,	used)).
car((4, 	b2,	blue,	13500,	84,	used)).
car((5, 	b6,	yellow,	18500,	65,	new)).
car((6, 	b6,	red,	12000,	382,	new)).
car((7, 	b0,	red,	6500,	87,	used)).
car((8, 	b4,	silver,	18500,	139,	new)).
car((9, 	b9,	yellow,	33500,	89,	new)).
car((10,	b4,	brown,	5500,	172,	used)).
car((11,	b9,	black,	9500,	391,	new)).
car((12,	b7,	black,	33000,	256,	used)).
car((13,	b5,	blue,	45500,	286,	new)).
car((14,	b1,	green,	9500,	280,	used)).
car((15,	b6,	yellow,	11000,	179,	used)).
car((16,	b8,	grey,	47000,	242,	used)).
car((17,	b2,	grey,	11000,	311,	used)).
car((18,	b3,	orange,	14000,	179,	used)).
car((19,	b2,	blue,	13000,	150,	new)).
car((20,	b3,	red,	11500,	64,	new)).
car((21,	b6,	blue,	8000,	278,	used)).
car((22,	b7,	white,	29000,	350,	used)).
car((23,	b5,	red,	44000,	248,	used)).
car((24,	b2,	blue,	48000,	120,	used)).
car((25,	b1,	green,	18500,	344,	new)).
car((26,	b1,	black,	35000,	149,	used)).
car((27,	b5,	grey,	38500,	93,	new)).
car((28,	b4,	orange,	29000,	398,	new)).
car((29,	b9,	blue,	9000,	193,	new)).
car((30,	b0,	red,	16500,	343,	used)).
car((31,	b1,	green,	15000,	353,	new)).
car((32,	b7,	grey,	6000,	74,	new)).
car((33,	b6,	black,	11000,	389,	new)).
car((34,	b0,	orange,	48000,	392,	new)).
car((35,	b8,	yellow,	45500,	248,	used)).
car((36,	b3,	black,	41500,	207,	new)).
car((37,	b7,	red,	12000,	297,	used)).
car((38,	b0,	green,	43000,	256,	used)).
car((39,	b1,	red,	30000,	341,	new)).
car((40,	b4,	grey,	13000,	209,	new)).
car((41,	b7,	blue,	33000,	183,	used)).
car((42,	b4,	silver,	9500,	169,	used)).
car((43,	b0,	white,	29500,	312,	new)).
car((44,	b6,	green,	18000,	171,	new)).
car((45,	b4,	brown,	8000,	77,	new)).
car((46,	b5,	brown,	23500,	373,	new)).
car((47,	b0,	brown,	16000,	380,	new)).
car((48,	b7,	blue,	23000,	211,	used)).
car((49,	b0,	orange,	17500,	162,	new)).
car((50,	b7,	red,	16500,	166,	used)).
car((51,	b9,	black,	7000,	234,	used)).
car((52,	b8,	brown,	37500,	141,	new)).
car((53,	b9,	grey,	37000,	235,	new)).
car((54,	b9,	brown,	9500,	141,	new)).
car((55,	b9,	white,	11500,	125,	new)).
car((56,	b0,	green,	31000,	313,	new)).
car((57,	b1,	silver,	48500,	88,	used)).
car((58,	b3,	brown,	21500,	215,	used)).
car((59,	b5,	red,	35500,	256,	used)).
car((60,	b1,	green,	43500,	196,	used)).
car((61,	b2,	red,	49000,	176,	used)).
car((62,	b8,	grey,	9000,	62,	new)).
car((63,	b1,	green,	25500,	75,	used)).
car((64,	b0,	blue,	15000,	304,	new)).
car((65,	b4,	green,	34000,	125,	new)).
car((66,	b9,	brown,	9000,	185,	used)).
car((67,	b3,	green,	27000,	315,	used)).
car((68,	b4,	red,	49000,	399,	used)).
car((69,	b0,	grey,	39000,	186,	new)).
car((70,	b4,	white,	6500,	236,	new)).
car((71,	b3,	brown,	44000,	61,	new)).
car((72,	b3,	green,	49500,	71,	used)).
car((73,	b6,	brown,	43500,	324,	used)).
car((74,	b2,	yellow,	8000,	190,	used)).
