#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
	srand(time(NULL));
	FILE * fd;
	if(argc!=3)
	{
		printf("Error: wrong arguments\n");
		exit(-1);
	}
	int k=atoi(argv[1]);
	int m=atoi(argv[2]);
	int filenumber=0;
	char filename[50], tempfilename[50];
	int i, j;

	sprintf(tempfilename, "graph_paths");
	sprintf(filename, "%s.P", tempfilename);

	while((fd=fopen(filename, "r"))!=NULL)
	{
		fclose(fd);
		sprintf(filename, "%s%d.P", tempfilename, filenumber);
		filenumber++;
	}

	if ((fd=fopen(filename, "w"))==NULL)
	{
		printf("Error: at file creation\n");
		exit(-1);
	}

	fprintf(fd, "%% Graph %d - %d\n\n", k, m);

	for(i=0; i<k; i++)
	{
		for(j=0; j<m; j++)
		{
			fprintf(fd, "edge((");
			if(i==0)
				fprintf(fd, "a,\t");
			else
				fprintf(fd, "b%d,\t", i-1);
			fprintf(fd, "v%d%d,\t", i, j);
			fprintf(fd, "%d)).\n", rand()%500);
		}
		for(j=0; j<m; j++)
		{
			fprintf(fd, "edge((");
			fprintf(fd, "v%d%d,\t", i, j);
			if(i==k-1)
				fprintf(fd, "z,\t");
			else
				fprintf(fd, "b%d,\t", i);
			fprintf(fd, "%d)).\n", rand()%500);

		}
	}

	fclose(fd);
	exit(0);
}
